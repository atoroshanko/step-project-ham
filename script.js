// 1)
const getParagraf = document.querySelectorAll("p");
getParagraf.forEach((element) => {
  element.style.backgroundColor = "#ff0000";
});
// 2)
const optionsList = document.getElementById("optionsList");
console.log(optionsList);
console.log(optionsList.childNodes);
console.log(optionsList.parentNode);
// 3)
const getParagraphById = document.getElementById("testParagraph");
getParagraphById.innerHTML = "This is a paragraph";
// 4), 5)
const getClassMainHeader = document.querySelector(".main-header").children;
console.log(getClassMainHeader);
for (const child of getClassMainHeader) {
  child.classList.add("nav-item");
}
// 6)
const section = document.querySelectorAll(".section-title");
section.forEach((element) => {
  element.classList.remove("section-title");
});
console.log(section);
