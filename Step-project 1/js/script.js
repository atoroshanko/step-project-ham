const serviceTab = document.querySelectorAll(".service-nav-item");
const serviceItem = document.querySelectorAll(".tab-content");

serviceTab.forEach((element) => {
  element.addEventListener("click", function () {
    let tabId = element.getAttribute("data-tab");
    let tabContent = document.querySelector(tabId);

    if (!element.classList.contains("active-service")) {
      serviceTab.forEach((element) => {
        element.classList.remove("active-service");
      });

      serviceItem.forEach((element) => {
        element.classList.add("hide");
      });

      element.classList.add("active-service");
      tabContent.classList.remove("hide");
    }
  });
});

const imgWrapper = document.querySelector(".our-work-wrapper");
const btnLoadMore = document.getElementById("load-more");
btnLoadMore.addEventListener("click", function () {
  imgWrapper.style.overflow = "visible";
  imgWrapper.style.maxHeight = "1500px";
  btnLoadMore.remove();
});

const tabsTypeDesign = document.querySelectorAll(".type-design-item");
tabsTypeDesign.forEach((element) => {
  element.addEventListener("click", function () {
    const allDivWithImg = document.querySelectorAll(".our-work-item");
    allDivWithImg.forEach((element) => {
      element.classList.add("hide");
    });
    let dataAttribute = element.getAttribute("data-img");
    let selectedImg = document.querySelectorAll(dataAttribute);
    selectedImg.forEach((element) => {
      element.classList.remove("hide");
    });
  });
});

const imgList = document.querySelectorAll(".people-reviews-item");
const peopleReviewsContent = document.querySelectorAll(".people-reviews");
imgList.forEach((element) => {
  element.addEventListener("click", function () {
    peopleReviewsContent.forEach((element) => {
      imgList.forEach((element) => {
        element.style.transform = "translateY(0px)";
      });
      element.classList.add("opacity");
    });
    const nameAttribute = element.getAttribute("data-name");
    const selectedRewies = document.querySelector(nameAttribute);
    selectedRewies.classList.remove("opacity");
    element.style.transform = "translateY(-15px)";
  });
});

// const btnForward = document.getElementById("btn-click-forward");
// const btnBack = document.getElementById("btn-click-back");

// let indexValue = 1;
// btnForward.addEventListener("click", function () {
//   let x;
//   for (x = 0; x < peopleReviewsContent.length; x++) {
//     peopleReviewsContent[x].classList.add("opacity");
//   }
//   indexValue++;
//   if (indexValue > peopleReviewsContent.length) {
//     indexValue = 1;
//   }
//   peopleReviewsContent[indexValue - 1].classList.remove("opacity");
// });
